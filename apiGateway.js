const createUser = async ({body}) => {
    const {userName} = body;

    return {
        statusCode: 200,
        body: JSON.stringify({
            message: `User ${userName} was successfully created!`,
        })
    };
};

const updateUser = async ({pathParameters}) => {
    const {id} = pathParameters;

    return {
        statusCode: 200,
        body: JSON.stringify({
            message: `User with id ${id} was successfully updated!`
        })
    };
};

const getUsers = async ({queryStringParameters}) => {
    const role = queryStringParameters ? queryStringParameters.role : 'admin';
    return {
        statusCode: 200,
        body: JSON.stringify({
            message: `Users with role ${role} were found!`
        })
    };
};

module.exports = {
    createUser,
    updateUser,
    getUsers,
};
