const uuid = require('uuid');
const DynamoDB = require('aws-sdk/clients/dynamodb');

const dynamoDb = new DynamoDB.DocumentClient();

const savePostsToDB = (event, context, callback) => {
    const timestamp = new Date().getTime();
    const data = JSON.parse(event.body);

    data.posts.forEach((post)=>{
        const commomParams = {
            headers: { 'Content-Type': 'text/plain' },
            body: 'Couldn\'t create the post.',
        };
        if (typeof post.name !== 'string') {
            console.error('Validation Failed');
            callback(null, {
                statusCode: 400,
                ...commomParams
            });
            return;
        }

        const params = {
            TableName: process.env.DYNAMODB_TABLE,
            Item: {
                id: uuid.v1(),
                createdAt: timestamp,
                updatedAt: timestamp,
                ...post
            },
        };

        dynamoDb.put(params, (error) => {
            if (error) {
                console.log(JSON.stringify(error));
                console.error(error);
                callback(null, {
                    statusCode: error.statusCode || 501,
                    ...commomParams
                });
                return;
            }

            const response = {
                statusCode: 200,
                body: JSON.stringify(params.Item),
            };
            callback(null, response);
        });
    })
};

const getPostsFromDB = (event, context, callback) => {
    const params = {
        TableName: process.env.DYNAMODB_TABLE,
    };

    dynamoDb.scan(params, (error, result) => {
        if (error) {
            console.error(error);
            callback(null, {
                statusCode: error.statusCode || 501,
                headers: { 'Content-Type': 'text/plain' },
                body: 'Couldn\'t fetch the posts.',
            });
            return;
        }

        const response = {
            statusCode: 200,
            body: JSON.stringify(result.Items),
        };
        callback(null, response);
    });
};

module.exports = {
    savePostsToDB,
    getPostsFromDB
};
