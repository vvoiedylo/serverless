const S3 = require('aws-sdk/clients/s3');
const xlsx = require('node-xlsx');
const axios = require('axios');

const handleUploadToS3 = (event) => {
    const CREATE_POST_URL = 'https://vx85bpiizg.execute-api.us-east-1.amazonaws.com/dev/posts';
    const s3 = new S3({apiVersion: '2006-03-01'});

    event.Records.forEach((record) => {
        const params = {
            Bucket: record.s3.bucket.name,
            Key: record.s3.object.key
        };

        const file = s3.getObject(params).createReadStream();
        const buffers = [];

        file.on('data', (data) => {
            buffers.push(data);
        });

        file.on('end', async () => {
            const buffer = Buffer.concat(buffers);
            const [workbook] = xlsx.parse(buffer);
            const posts = workbook.data.map(([name, description]) => ({name, description}));

            await axios({
                method: 'post',
                url: CREATE_POST_URL,
                data: JSON.stringify({posts})
            });
        });
    });
};

module.exports = {
    handleUploadToS3,
};
